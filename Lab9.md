# Lab9

Website - https://www.nexusmods.com

## 1. Forgot Password

| Test step  | Result |
|---|---|
| Forgot Password | Recovery via email page opened |
| Random Email Input | "Success. If your username or email address exists in our database, you will receive a password recovery link at your email address in a few minutes." |
| Personal Email Input | "Success. If your username or email address exists in our database, you will receive a password recovery link at your email address in a few minutes." Received an email with instructions and a recovery URL |
| Clicked URL | Password reset page opened |
| Password doesn't follow requirments | Unable to procees |
| Confirm password is differnt | Unable to procees |
| Password is acceptable | "SUCCESS Your password has been changed successfully." |
| Login with new password | OK |

## 2. Login

| Test step  | Result |
|---|---|
| Invalid Credentials | "Error. Invalid Login or password" |
| Valid Credentials | OK |
| login=' OR 1=1 -- password=' OR 1=1 -- | "Sorry, you have been blocked" page opened|

## 3. Search

| Test step  | Result |
|---|---|
| Search "Autumn Leaves" | OK |
| Search ' OR 1=1 -- | "Sorry, you have been blocked" page opened |
| Search <script>alert(1)</script> | "Sorry, you have been blocked" page opened|
